import { createStore, combineReducers } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import { AsyncStorage } from "react-native";

import { dataReducer } from "./data";

const rootReducer = combineReducers({
  data: dataReducer,
});

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer);
export const persistor = persistStore(store);

console.log(store.getState());
store.subscribe(() => console.log(store.getState()));

export default store;
