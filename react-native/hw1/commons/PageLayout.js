import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { CustomText } from "../components";

export const PageLayout = ({
  children,
  heading,
  rightIcon,
  rightIconSize,
  rightIconAction,
  leftIcon,
  leftIconAction,
  leftIconSize,
}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={leftIconAction} style={styles.leftIcon}>
          <Ionicons name={leftIcon} size={leftIconSize} color="white" />
        </TouchableOpacity>
        <CustomText weight="semi" style={styles.headingText}>
          {heading}
        </CustomText>
        <TouchableOpacity onPress={rightIconAction} style={styles.rightIcon}>
          <Ionicons name={rightIcon} size={rightIconSize} color="white" />
        </TouchableOpacity>
      </View>
      <View style={styles.body}>{children}</View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FF7676",
  },
  header: {
    backgroundColor: "#FF7676",
    width: "100%",
    justifyContent: "center",
  },

  rightIcon: {
    position: "absolute",
    right: 16,
    marginVertical: 22,
  },
  leftIcon: {
    position: "absolute",
    marginVertical: 22,
    width: 48,
    height: 48,
    alignItems: "center",
    justifyContent: "center",
  },
  headingText: {
    fontSize: 18,
    textAlign: "center",
    justifyContent: "center",
    marginVertical: 22,
    color: "white",
    lineHeight: 22,
  },
  body: {
    flex: 1,
    backgroundColor: "white",
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    paddingTop: 16,
    paddingHorizontal: 16,
  },
});
