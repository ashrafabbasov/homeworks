import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { CustomText } from "./CustomText";
import { Ionicons } from "@expo/vector-icons";
export const SingleItemEdit = ({
  name,
  unit,
  amount,
  opacity,
  onPressEdit,
  onPressDelete,
}) => {
  return (
    <View>
      <View style={styles.container}>
        <TouchableOpacity onPress={onPressEdit} style={styles.editIcon}>
          <Ionicons
            style={{ opacity }}
            name="md-create"
            size={20}
            color="white"
          />
        </TouchableOpacity>
        <View style={styles.inside}>
          <CustomText style={{ fontSize: 14 }}>{name}</CustomText>
          <CustomText
            style={{ fontSize: 14 }}
          >{`x${amount} ${unit}`}</CustomText>
        </View>
        <TouchableOpacity onPress={onPressDelete} style={styles.deleteIcon}>
          <Ionicons name="ios-close" size={40} color="white" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    borderWidth: 2,
    borderRadius: 27,
    borderColor: "#FFE194",
    flexDirection: "row",
    height: 44,
    marginTop: 15,
    justifyContent: "space-between",
  },
  inside: {
    flexDirection: "row",
    paddingHorizontal: 14,
    width: "75%",
    alignItems: "center",
    justifyContent: "space-between",
  },
  editIcon: {
    borderRadius: 35,
    backgroundColor: "#FFD976",
    height: 40,
    width: 40,
    justifyContent: "center",
    alignItems: "center",
  },
  deleteIcon: {
    right: 0,
    borderRadius: 40,
    backgroundColor: "#FF7676",
    height: 40,
    width: 40,
    justifyContent: "center",
    alignItems: "center",
  },
});
