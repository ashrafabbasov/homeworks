import React from "react";
import {
  Text,
  View,
  StyleSheet,
  Button,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform,
} from "react-native";
import { CustomText } from "./CustomText";
export const SecondaryButton = ({
  title,
  onPress,
  customWidth,
  customOpacity,
}) => {
  const Touchable =
    Platform.OS === "android" ? TouchableNativeFeedback : TouchableOpacity;
  return (
    <View
      style={(styles.container, { width: customWidth, opacity: customOpacity })}
    >
      <Touchable onPress={onPress}>
        <View style={styles.btn}>
          <CustomText weight="bold" style={styles.title}>
            {title}
          </CustomText>
        </View>
      </Touchable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 14,
    backgroundColor: "white",
  },
  btn: {
    backgroundColor: "#eee",
    height: 42,
    borderRadius: 45,
    justifyContent: "center",
  },
  title: {
    textAlign: "center",
    fontSize: 12,
  },
});
