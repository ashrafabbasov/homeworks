import React from "react";

import { View, Text, TextInput, StyleSheet } from "react-native";
import { CustomText } from "./CustomText";
import { TouchableOpacity } from "react-native-gesture-handler";

export const CustomField = ({
  Title,
  onChangeText,
  width,
  maxLength,
  value,
}) => {
  return (
    <View style={[styles.container, { width }]}>
      <CustomText weight="semi" style={styles.title}>
        {Title}
      </CustomText>

      <TextInput
        onChangeText={onChangeText}
        style={styles.input}
        maxLength={maxLength}
        value={value}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
  },
  title: {
    fontSize: 12,
    fontWeight: "600",
    textAlign: "center",
    color: "#303234",
    paddingBottom: 9,
  },
  input: {
    backgroundColor: "#eee",
    borderRadius: 45,
    height: 42,
    textAlign: "center",
    fontSize: 18,
    color: "#303234",
  },
});
