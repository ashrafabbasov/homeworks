export const findNumberofBought = (item) => {
  let count = 0;
  for (let i = 0; i < item.length; i++) {
    if (item[i].bought) {
      count++;
    }
  }
  return count;
};
