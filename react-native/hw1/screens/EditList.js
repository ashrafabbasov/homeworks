import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from "react-native";
import { connect } from "react-redux";
import { Ionicons } from "@expo/vector-icons";

import { PageLayout } from "../commons/PageLayout";
import {
  PrimaryButton,
  CustomText,
  SingleItemEdit,
  CustomField,
  SecondaryButton,
} from "../components";

import { getLists, deleteItem, addItem, updateItem } from "../redux/data";

const mapStateToProps = (state) => ({
  lists: getLists(state),
});

export const EditList = connect(mapStateToProps, {
  deleteItem,
  addItem,
  updateItem,
})(({ route, lists, navigation, deleteItem, addItem, updateItem }) => {
  const {
    params: { componentID, componentName, listType },
  } = route;
  const componentItems = getComponentItems(lists, componentID, listType);
  const [fields, setFields] = useState({
    name: "",
    amount: "0",
    unit: "pkg",
  });
  const [editMode, setEditMode] = useState(false);
  const [updateID, setUpdateID] = useState("");
  const resetFields = () => setFields({ name: "", amount: "0", unit: "pkg" });

  const fieldChangeHandler = (name, value) => {
    setFields((fields) => ({
      ...fields,
      [name]: value,
    }));
  };
  console.log(updateID);

  const changeAmount = (op, unit) => {
    if (unit !== "kg") {
      if (op === "increase") {
        setFields((fields) => ({
          ...fields,
          amount: (parseInt(fields.amount) + 1).toFixed(),
        }));
      } else {
        setFields((fields) => ({
          ...fields,
          amount: (parseInt(fields.amount) - 1).toFixed(),
        }));
      }
    } else {
      if (op === "increase") {
        setFields((fields) => ({
          ...fields,
          amount: (parseFloat(fields.amount) + 0.1).toFixed(1),
        }));
      } else {
        setFields((fields) => ({
          ...fields,
          amount: (parseFloat(fields.amount) - 0.1).toFixed(1),
        }));
      }
    }
  };
  return (
    <PageLayout
      rightIcon={"md-save"}
      rightIconSize={24}
      rightIconAction={() => {
        navigation.navigate("SingleList");
      }}
      heading={`${componentName}`}
      leftIcon={"md-arrow-back"}
      leftIconSize={28}
      leftIconAction={() => {
        navigation.goBack();
      }}
    >
      <View style={styles.container}>
        <View style={styles.itemContents}>
          <View style={styles.fields}>
            <CustomField
              value={fields.name}
              onChangeText={(value) => fieldChangeHandler("name", value)}
              Title="position name"
              width="65%"
              maxLength={19}
            />
            <View style={styles.counter}>
              <CustomText weight="semi" style={styles.title}>
                count
              </CustomText>
              <TextInput
                value={fields.amount.toString()}
                style={{
                  textAlign: "center",
                  fontWeight: "bold",
                  width: "29%",
                  height: 42,
                  backgroundColor: "#eee",
                  borderRadius: 45,
                }}
                onChangeText={(value) => fieldChangeHandler("amount", value)}
                maxLength={3}
                keyboardType="numeric"
              />
              <TouchableOpacity
                onPress={() => changeAmount("decrease", fields.unit)}
                style={{ position: "absolute", bottom: 9, left: 13 }}
              >
                <Ionicons name="md-remove" size={20} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => changeAmount("increase", fields.unit)}
                style={{ position: "absolute", bottom: 9, left: 73 }}
              >
                <Ionicons name="md-add" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.unitChooser}>
            <SecondaryButton
              customOpacity={fields.unit === "pkg" ? 1 : 0.3}
              onPress={() => fieldChangeHandler("unit", "pkg")}
              title="pkg"
              customWidth="23%"
            />
            <SecondaryButton
              customOpacity={fields.unit === "kg" ? 1 : 0.3}
              onPress={() => fieldChangeHandler("unit", "kg")}
              title="kg"
              customWidth="23%"
            />
            <SecondaryButton
              customOpacity={fields.unit === "litre" ? 1 : 0.3}
              onPress={() => fieldChangeHandler("unit", "litre")}
              title="litre"
              customWidth="23%"
            />
            <SecondaryButton
              customOpacity={fields.unit === "bott" ? 1 : 0.3}
              onPress={() => fieldChangeHandler("unit", "bott")}
              title="bott"
              customWidth="23%"
            />
          </View>
        </View>

        {!!editMode ? (
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <PrimaryButton
              onPress={() => {
                setEditMode(false);
                resetFields();
                setUpdateID("");
              }}
              cheight={42}
              title="cancel"
              style={{ width: "48%", opacity: 0.5 }}
            />
            <PrimaryButton
              onPress={() => {
                updateItem({ ...fields, componentID, updateID, listType });
              }}
              cheight={42}
              title="update"
              style={{ width: "48%" }}
            />
          </View>
        ) : (
          <PrimaryButton
            onPress={() => {
              addItem({ componentID, listType, ...fields });
              resetFields();
            }}
            cheight={42}
            title="add to list"
          />
        )}

        <View style={styles.line}></View>
        <ScrollView>
          {componentItems.map((item) => (
            <SingleItemEdit
              unit={item.unit}
              amount={item.amount}
              name={item.name}
              key={item.id}
              opacity={item.id === updateID ? 0.5 : 1}
              onPressDelete={() => {
                deleteItem({ listType, componentID, itemID: item.id });
              }}
              onPressEdit={() => {
                setEditMode(true);
                setFields({
                  name: item.name,
                  amount: item.amount,
                  unit: item.unit,
                });
                setUpdateID(item.id);
              }}
            />
          ))}
        </ScrollView>
      </View>
    </PageLayout>
  );
});

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  button: {
    width: "18%",
  },
  line: {
    height: 2,
    backgroundColor: "#e5e5e5",
    marginVertical: 19,
  },
  fields: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  counter: {
    flexDirection: "column",
    width: "100%",
    marginLeft: 18,
  },
  title: {
    fontSize: 12,
    fontWeight: "600",
    color: "#303234",
    paddingBottom: 9,
    width: "29%",
    textAlign: "center",
  },
  unitChooser: {
    marginVertical: 14,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});

const getComponentItems = (lists, componentID, listType) => {
  for (let item of lists) {
    if (item.listType === listType) {
      for (let i = 0; i < item.components.length; i++) {
        if (item.components[i].id === componentID) {
          return item.components[i].items;
        }
      }
    }
  }
};
