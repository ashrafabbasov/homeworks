import React from "react";
import { connect } from "react-redux";

import { PageLayout } from "../commons/PageLayout";
import DrawerIcon from "../assets/drawer.png";
import ListCard from "../components/ListCard";
import { getLists, toggleBought, deleteList } from "../redux/data";
import { findNumberofBought } from "../utils/functions";
import { ScrollView } from "react-native";

const mapStateToProps = (state) => ({
  lists: getLists(state),
});

export const OneTimeLists = connect(mapStateToProps, {
  toggleBought,
  deleteList,
})(({ props, lists, navigation, toggleBought, deleteList }) => {
  const oneTimeLists = getOneTimeLists(lists);

  return (
    <PageLayout
      rightIcon={"ios-menu"}
      rightIconSize={34}
      navigation={navigation}
      heading="One Time"
      rightIconAction={() => {
        navigation.openDrawer();
      }}
    >
      <ScrollView>
        {oneTimeLists.components.map((component) => {
          return (
            <ListCard
              onPress={() => {
                const componentID = component.id;
                const componentName = component.name;
                const listType = "onetime";
                navigation.navigate("SingleList", {
                  componentID,
                  componentName,
                  listType,
                });
              }}
              onLongPress={() =>
                deleteList({ listType: "onetime", componentID: component.id })
              }
              key={component.id}
              title={component.name}
              numberOfBought={
                component.items.length == 0
                  ? 0
                  : findNumberofBought(component.items)
              }
              totalNumber={component.items.length}
            />
          );
        })}
      </ScrollView>
    </PageLayout>
  );
});
const getOneTimeLists = (lists) => {
  for (let item of lists) {
    if (item.listType == "onetime") {
      return item;
    }
  }
};
