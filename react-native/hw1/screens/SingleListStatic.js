import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { connect } from "react-redux";

import { PageLayout } from "../commons/PageLayout";
import { PrimaryButton, CustomText, SingleItem } from "../components";
import { findNumberofBought } from "../utils/functions";
import { getLists, toggleBought, resetBought } from "../redux/data";
import { ScrollView } from "react-native-gesture-handler";

const mapStateToProps = (state) => ({
  lists: getLists(state),
});

export const SingleListStatic = connect(mapStateToProps, {
  toggleBought,
  resetBought,
})(({ route, toggleBought, lists, resetBought, navigation }) => {
  const {
    params: { componentID, componentName, listType },
  } = route;
  const componentItems = getComponentItems(lists, componentID, listType);

  return (
    <PageLayout
      rightIcon={"md-create"}
      rightIconSize={24}
      rightIconAction={() => {
        navigation.navigate("EditList", {
          componentID,
          listType,
          componentName,
        });
      }}
      heading={`${componentName}`}
      leftIcon={"md-arrow-back"}
      leftIconSize={28}
      leftIconAction={() => {
        navigation.goBack();
      }}
    >
      <View style={styles.container}>
        <View style={styles.header}>
          <PrimaryButton
            cFontSize={10}
            title="Reset"
            cheight={20}
            style={styles.button}
            onPress={() => {
              resetBought({ listType, componentID });
            }}
          />
          <CustomText style={{ fontSize: 14 }}>
            {findNumberofBought(componentItems)} / {componentItems.length}
          </CustomText>
        </View>
        <ScrollView>
          {componentItems.map((item) => (
            <SingleItem
              unit={item.unit}
              amount={item.amount}
              name={item.name}
              key={item.id}
              onLongPress={() => {
                toggleBought({
                  listType,
                  componentID: componentID,
                  itemID: item.id,
                });
              }}
              opacity={item.bought ? 0.3 : 1}
            />
          ))}
        </ScrollView>
      </View>
    </PageLayout>
  );
});

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  button: {
    width: "18%",
  },
});
const getComponentItems = (lists, componentID, listType) => {
  for (let item of lists) {
    if (item.listType === listType) {
      for (let i = 0; i < item.components.length; i++) {
        if (item.components[i].id === componentID) {
          return item.components[i].items;
        }
      }
    }
  }
};
