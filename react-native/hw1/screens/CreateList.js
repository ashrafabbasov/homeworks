import React, { useState } from "react";
import { Button, Text, View, StyleSheet, Alert } from "react-native";
import { connect } from "react-redux";

import { PageLayout } from "../commons/PageLayout";
import { getLists, addList } from "../redux/data";
import { CustomField, SecondaryButton, PrimaryButton } from "../components";

const mapStateToProps = (state) => ({
  lists: getLists(state),
});

export const CreateList = connect(mapStateToProps, { addList })(
  ({ addList, navigation }) => {
    const [listT, setListT] = useState("onetime");
    const [field, setField] = useState("");

    const submitHandler = () => {
      const nav = listT === "onetime" ? "OneTime" : "Regular";
      if (field.trim() === "") {
        Alert.alert(`Please, enter a list name`);
        return;
      }
      addList({
        listType: listT,
        name: field,
      });
      navigation.navigate(nav);
    };

    return (
      <PageLayout heading="New List">
        <CustomField
          value={field}
          onChangeText={(value) => setField(value)}
          Title="list name"
          maxLength={40}
        />
        <View style={styles.selectionButtons}>
          <SecondaryButton
            title="One Time"
            customOpacity={listT === "onetime" ? 1 : 0.5}
            customWidth="47%"
            onPress={() => {
              setListT("onetime");
            }}
          />
          <SecondaryButton
            title="Regular"
            customOpacity={listT === "regular" ? 1 : 0.5}
            customWidth="47%"
            onPress={() => {
              setListT("regular");
            }}
          />
        </View>
        <PrimaryButton
          style={{ marginTop: 14 }}
          cFontSize={14}
          cheight={42}
          title="Create List"
          onPress={() => {
            submitHandler();
          }}
        />
      </PageLayout>
    );
  }
);

const styles = StyleSheet.create({
  selectionButtons: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 14,
  },
});
