import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import {
  RegularLists,
  OneTimeLists,
  UserSettings,
  CreateList,
  EditList,
  SingleListStatic,
  SettingScreen,
} from "../screens";
const { Navigator, Screen } = createStackNavigator();

export const RootStack = () => (
  <Navigator>
    <Screen
      name="Regular"
      component={RegularLists}
      options={{
        animationEnabled: false,
        gestureEnabled: false,
        headerShown: false,
      }}
    />
    <Screen
      name="EditList"
      component={EditList}
      options={{
        animationEnabled: false,
        gestureEnabled: false,
        headerShown: false,
      }}
    />
    <Screen
      name="SingleList"
      component={SingleListStatic}
      options={{
        animationEnabled: false,
        gestureEnabled: false,
        headerShown: false,
      }}
    />
    <Screen
      name="Create"
      component={CreateList}
      options={{
        animationEnabled: false,
        gestureEnabled: false,
        headerShown: false,
      }}
    />
    <Screen
      name="Settings"
      component={SettingScreen}
      options={{
        animationEnabled: false,
        gestureEnabled: false,
        headerShown: false,
      }}
    />
    <Screen
      name="OneTime"
      component={OneTimeLists}
      options={{
        animationEnabled: false,
        gestureEnabled: false,
        headerShown: false,
      }}
    />
  </Navigator>
);
