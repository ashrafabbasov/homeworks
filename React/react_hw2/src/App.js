import React, {useState, useEffect} from 'react';
import { ProductCard } from './components/ProductCard';


function App() {

  const [data, setData] = useState([]);
  

  const getData = async(url) => {
    const resp = await fetch(url);
    const newData = await resp.json();
    setData(newData);
  }

  

  useEffect(()=>{

    getData('http://localhost:3001/products');

  },[])


  return (
    <div className="App">
      {data.map(({id,name,price,image,desc})=>(
        <ProductCard id={id} name={name} price={price} image={image} description={desc} id={id}  />
      ))}
      
    </div>
  );
}

export default App;
