import React, { useContext } from 'react';

import {ProductCard} from './ProductCard';



export const ProductsList = ({data,cart,fav}) => {

  

    
    
  
    return(

        <div className="App">
            {data.map(({id,name,price,image,description})=>(
                <ProductCard fav={fav} key={id} name={name} price={price} image={image} description={description} id={id} cart={cart} />
              ))}
        </div>
    
    )

}