import React, {useState, useEffect} from 'react';
import {ProductsList} from '../../components/ProductsList'

export const Homepage = () => {

    const [data, setData] = useState([]);

    const getData = async(url) => {
      const resp = await fetch(url);
      const newData = await resp.json();
      setData(newData);
    }
  
    useEffect(()=>{
      getData('http://localhost:3000/products');
  
    },[])

  

  

    


    return (
        <ProductsList fav={false} data={data} />
    )

}