import React from 'react';
import {NavLink} from 'react-router-dom'
import styled from 'styled-components'
export const Header = () => {

    return (
    <Container>
        <StyledNavLink exact to="/">Homepage</StyledNavLink>
        <StyledNavLink to="/cart">Cart</StyledNavLink>
        <StyledNavLink to="/favs">Favorites</StyledNavLink>
    </Container>
    )
}


const Container = styled.header`
    padding: 20px;
    text-align: center;
    border-bottom: 1px solid lightgrey;
    display: flex;
    justify-content: center;
    background-color: rgba(0,0,0,1);
`

const StyledNavLink = styled(NavLink)`
    width: 10%;
    border: 1px solid lightgrey;
    padding: 10px;
    
    color: gray;
    text-transform: uppercase;
    text-decoration: none;
    font-size: 14px;
    transition: all .3s ease;
    margin: 10px;

    &.active {
        border-color: white;
        color: white;
    }
`
